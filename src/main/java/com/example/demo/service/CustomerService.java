package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.AccountCommand;
import com.example.demo.model.Customer;
import com.example.demo.model.SavingsAccount;
import com.example.demo.repository.CustomerRepository;

@Service
public class CustomerService {
	
	private final CustomerRepository customerRepository;
	
	public CustomerService(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
	
	public Customer saveCustomer(Customer customer) {
		return this.customerRepository.save(customer);
	}
	
	public List<Customer> fetchAllCustomers(){
		return this.customerRepository.findAll();
	}
	
	public Customer findCustomerById(long customerId) {
		return this.customerRepository.findById(customerId).orElseThrow(() -> new IllegalArgumentException("Invalid customer id"));
	}
	
	public void deleteCustomer(long customerId) {
		this.customerRepository.deleteById(customerId);
	}
	
	public Customer updateSavingsAccount(long customerId, AccountCommand accountCommand) {
		Customer customer = this.customerRepository.findById(customerId).orElseThrow(() -> new IllegalArgumentException("Invalid customer id"));
		//logic
		Optional<SavingsAccount> optionalSavingsAccount = customer
																.getAccounts().stream()
																.filter(account -> account.getId() == accountCommand.getSavingsAccount().getId())
																.findAny();
		if(optionalSavingsAccount.isPresent()) {
			SavingsAccount existingAccount = optionalSavingsAccount.get();
			SavingsAccount passedSavingsAccount = accountCommand.getSavingsAccount();
			
			existingAccount.setBalance(passedSavingsAccount.getBalance());
			existingAccount.setBranch(passedSavingsAccount.getBranch());
			
			return this.customerRepository.save(customer);
		} else {
			SavingsAccount newSavingsAccount = accountCommand.getSavingsAccount();
			customer.addSavingsAccount(newSavingsAccount);
			return this.customerRepository.save(customer);
		}
	}
}
