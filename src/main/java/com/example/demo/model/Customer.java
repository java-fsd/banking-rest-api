package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GeneratorType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="customers")
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private String email;
	
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JsonManagedReference
	private List<SavingsAccount> accounts;
	
	private Customer() {}
	
	public Customer(String name, String email) {
		this.name = name;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<SavingsAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<SavingsAccount> accounts) {
		this.accounts = accounts;
	}

	public Long getId() {
		return id;
	}
	
	//setting both sides of the relationship
	public void addSavingsAccount(SavingsAccount savingsAccount) {
		if (this.accounts == null) {
			this.accounts = new ArrayList<SavingsAccount>();
		}
		this.accounts.add(savingsAccount);
		savingsAccount.setCustomer(this);
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", email=" + email + ", accounts=" + accounts + "]";
	}
}
