package com.example.demo.model;

public class AccountCommand {
	
	private long customerId;
	private SavingsAccount savingsAccount;
	
	public AccountCommand(long customerId, SavingsAccount savingsAccount) {
		super();
		this.customerId = customerId;
		this.savingsAccount = savingsAccount;
	}

	public long getCustomerId() {
		return customerId;
	}

	public SavingsAccount getSavingsAccount() {
		return savingsAccount;
	}

	@Override
	public String toString() {
		return "AccountCommand [customerId=" + customerId + ", savingsAccount=" + savingsAccount + "]";
	}
}
