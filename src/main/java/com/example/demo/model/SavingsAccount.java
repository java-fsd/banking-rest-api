package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="savings_account")
public class SavingsAccount {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String branch;
	
	private double balance;
	
	@ManyToOne
	@JoinColumn(name="customer_id", nullable = false)
	@JsonBackReference
	private Customer customer;
	
	private SavingsAccount() {}
	
	public SavingsAccount(String branchName, double balance) {
		this.balance = balance;
		this.branch = branchName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "SavingsAccount [id=" + id + ", branch=" + branch + ", balance=" + balance + "]";
	}

}
