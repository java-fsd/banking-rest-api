package com.example.demo.util;

import java.util.stream.IntStream;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

import com.example.demo.model.Customer;
import com.example.demo.model.SavingsAccount;
import com.example.demo.repository.CustomerRepository;
import com.github.javafaker.Faker;

@Configuration
public class BootstrapApplicationData implements ApplicationListener<ApplicationReadyEvent>{
	
	private CustomerRepository customerRepository;
	private Faker faker = new Faker();
	
	public BootstrapApplicationData(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		IntStream.range(0,  101).forEach(index -> {
			Customer customer = new Customer(faker.name().firstName(), faker.internet().emailAddress());
			
			IntStream.range(1, faker.number().numberBetween(2, 4)).forEach(value -> {
				SavingsAccount savingsAccount = new SavingsAccount(faker.country().capital(), faker.number().randomDouble(2, 10000, 25000));
				customer.addSavingsAccount(savingsAccount);
			});
			this.customerRepository.save(customer);
		});
	}
}
