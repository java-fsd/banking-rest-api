package com.example.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.AccountCommand;
import com.example.demo.model.Customer;
import com.example.demo.service.CustomerService;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {
	private CustomerService customerService;
	
	public CustomerRestController(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@PostMapping
	public Customer saveCustomer(@RequestBody Customer customer) {
		return this.customerService.saveCustomer(customer);
	}
	
	@GetMapping
	public List<Customer> fetchAllCustomers(){
		return this.customerService.fetchAllCustomers();
	}
	

	@GetMapping("/{id}")
	public Customer fetchCustomerById(@PathVariable long customerId){
		return this.customerService.findCustomerById(customerId);
	}
	
	@DeleteMapping("/{id}")
	public void deleteCustomerById(@PathVariable long customerId){
		this.customerService.deleteCustomer(customerId);
	}
	
	@PutMapping("/{id}")
	public Customer updateAccount(@PathVariable("id") long id, @RequestBody AccountCommand accountCommand) {
		return this.customerService.updateSavingsAccount(id, accountCommand);
	}

}
